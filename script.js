// 'use strict';
import ContactsApi from "./ContactsApi.js";

const EDIT_BTN_CLASS = 'edit-contact';
const DELETE_BTN_CLASS = 'delete-contact';

const contactForm = document.querySelector('.contacts-form');
const userName = document.querySelector('#name');
const userSurName = document.querySelector('#surName');
const userPhone = document.querySelector('#phone');
const addBtn = document.querySelector('#addItem');
const deleteBtn = document.querySelector('#deleteContact');
const contactBody = document.querySelector('#contactTable tbody');
const contactTemplate = document.querySelector('#contactTemplate');
const hidInput = contactForm.querySelector('input[type=hidden]');

let contactList = [];

addBtn.addEventListener('click', onAddBtnClick);
contactBody.addEventListener('click', onContactBodyClick);

ContactsApi.getList()
    .then((data) => {
        contactList = data;
        renderContactItems(data);
    })
    .catch(showError);

function onAddBtnClick(e) {
    e.preventDefault();
    const contact = getMessage();
    if (!isValid(contact)) {
        alert('Invalid field!');
        return;
    }
    if (contact.id) {
        
        ContactsApi.update(contact.id, contact)
            .then(ContactsApi.getList)
            .then(renderContactItems)
            .then(clearForm)
            .catch(showError);
    } else {
        ContactsApi.create(contact)
            .then(newContact => {
                contactList.push(newContact);
                addContactItem(newContact);
                clearForm();
            })
            .catch(showError);
    }
}

function onContactBodyClick(e) {
    const contactEl = getContactItem(e.target);
    const id = contactEl.dataset.id;
    const contact = contactList.find(contactItem => contactItem.id === id);

    if (e.target.classList.contains(DELETE_BTN_CLASS)) {
        ContactsApi.delete(id).catch(showError);
        contactEl.remove();
        return;
    }
    if (e.target.classList.contains(EDIT_BTN_CLASS)) {
        fillForm(contact);
        return;
    }
}

function getContactItem(el) {
    return el.closest('tr');
}

function fillForm(contact) {
    userName.value = contact.firstName;
    userSurName.value = contact.lastName;
    userPhone.value = contact.phone;
    hidInput.value = contact.id;
}

function getMessage() {
    const contact = contactList.find(contactItem => contactItem.id === hidInput.value) || {};

    const message = {
        ...contact,
        firstName: userName.value,
        lastName: userSurName.value,
        phone: userPhone.value,
    }
    return message;
}

function isValid(data) {
    let isNameValid = /^[A-Za-zА-Яа-яЁё]+$/.test(data.name);
    let isSurNameValid = /^[A-Za-zА-Яа-яЁё]+$/.test(data.surName);
    let isPhoneValid = /^[0-9]+$/.test(data.phone);

    return isNameValid && isSurNameValid && isPhoneValid ? true : false;
};

function renderContactItems(data) {
    const html = data.map(generateContactHtml).join('');

    contactBody.innerHTML = html;
}

function addContactItem(data) {
    const contactItem = generateContactHtml(data);
    contactBody.insertAdjacentHTML('beforeend', contactItem);
}

function generateContactHtml(data) {
    return contactTemplate
        .innerHTML
        .replace('{name}', data.firstName)
        .replace('{surname}', data.lastName)
        .replace('{phone}', data.phone)
        .replace('{id}', data.id);
}

function clearForm() {
    contactForm.reset();
}

function showError(e) {
    alert(e.message);
}